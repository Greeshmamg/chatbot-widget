import React, {useState, useRef, useEffect} from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import send_icon from '../../img/send_icon.png';
import { addQueryToConversation } from '../../store/actions/conversationActions';

const QueryInputContainer =  styled.div`
    position: relative;
`;

const QueryInputWrapper =  styled.div`
    display: flex;
    flex-direction: row;
    bottom: 0;
    padding: 2px;
    width: 100%;
    background-color: ${props => props.theme.colors.white};
    box-sizing: border-box;
    border-top: 1px solid ${props=>props.theme.colors.midGrey};
    border-bottom: 1px solid ${props=>props.theme.colors.midGrey};
    border-bottom-left-radius: 4px;
    border-bottom-right-radius: 4px; 
`;

const Form = styled.form`
    width: 100%;
    position: relative;
`;

const QueryInput = styled.input.attrs({
    placeholder: 'Type you question here...',
})`
    margin: 10px auto;
    padding: 5px 15px;
    position: relative;
    height: 35px;
    width: 100%;
    border: none;
    outline: none;
    box-sizing: border-box;
    font-size: 18px;
`;

// const SuggestionsContainer = styled.div`
//     margin: 0;
//     padding: 0;
//     position: absolute;
//     width: 100%;
//     box-sizing: border-box;
//     font-size: 18px;
//     background-color: ${props=>props.theme.colors.white};
//     border: 1px solid ${props=>props.theme.colors.midGrey};
// `;

// const SuggestionSpan = styled.div`
//     padding: 5px 10px;
//     width: 100%;
//     border: none;
// `;

const SendBtnWrapper =  styled.div`
    position: relative;
    align-self: right;
    z-index: 2;
    padding: 8px;
    width: 24px;
`;

const SendBtn =  styled.img`
    position: absolute;
    top: 0;
    bottom: 0;
    margin: auto;
    cursor: pointer;
    height: ${ (props) => props.active ? '24px' : 0 };
    width: ${ (props) => props.active ? '24px' : 0 };
    transition: all .3s ease-in-out;
`;

const ChatInput = ({display, sendUserQuery}) => {

    const [userQuery, setUserQuery] = useState('');

    const inputFocusRef = useRef();

    const focusInput = () => {
        const {current} = inputFocusRef;
        current.focus(); 
    }

    useEffect(() => {
        if (!display) { setUserQuery('') };
        focusInput();
    },[display]);

    const handleSubmit = (e) => {
        e.preventDefault();
        if (userQuery !== '') { sendUserQuery(userQuery) };
        setUserQuery('');
    }

    return (
        <>
            <QueryInputContainer>
                <QueryInputWrapper>
                    <Form onSubmit={handleSubmit}>
                        <QueryInput value={userQuery} onChange={(e)=>{ setUserQuery(e.target.value) }} ref={inputFocusRef} />
                    </Form>
                    <SendBtnWrapper><SendBtn src={send_icon} alt={''} onClick={handleSubmit} active={(userQuery !== '')}/></SendBtnWrapper>
                </QueryInputWrapper>
            </QueryInputContainer>
        </>
    )
};

const mapStateToProps = ({chatbot}) => ({
    display: chatbot.display,
})

const mapDispatchToProps = (dispatch) => ({
    sendUserQuery: (userQuery) => { dispatch(addQueryToConversation(userQuery)) },
})

export default connect(mapStateToProps, mapDispatchToProps)(ChatInput);