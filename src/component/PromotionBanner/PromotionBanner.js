import React from 'react';
import styled from 'styled-components';
import { PROMOTION_BANNER_TEXT } from '../../utils/Constants';

const BannerWrapper = styled.div`
    background: ${props=>props.theme.colors.darkRed};
    border: 2px solid ${props=>props.theme.colors.darkRed};
    border-radius: 0 0 4px 4px;
    align-items: center;
    justify-content: space-between;
    height: 70px;
    padding-left: 15px;
    color: ${props=>props.theme.colors.lightGrey};
    display: flex;
`;

const BannerText = styled.div`
    font-size: 16px;
`;

const PromotionBanner = () => {
    return (
        <BannerWrapper>
            <BannerText>
                {PROMOTION_BANNER_TEXT}
            </BannerText>
        </BannerWrapper>
    )
};

export default PromotionBanner;