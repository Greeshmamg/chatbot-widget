import React from 'react';
import {connect} from 'react-redux';
import styled, {keyframes, css} from 'styled-components';
import { toggleChatbot, startChatbot } from '../../store/actions/chatbotActions';
import ChatModal from '../ChatModal';

const bounceAnimation = keyframes`
    to {
        transform: scale(1.2);
    }
`;

const ChatBtn = styled.button`
    /* background: ${ (props) => (props.clicked) ? props.theme.colors.white : props.theme.colors.red};
    color: ${ (props) => (props.clicked) ? props.theme.colors.red : props.theme.colors.white}; */
    background: ${ (props) => props.theme.colors.red};
    color: ${ (props) => props.theme.colors.white};
    display: ${ (props) => props.clicked ? 'none' : 'inline-block' };
    font-size: 32px;
    height: 58px;
    width: 58px;
    position: fixed;
    bottom: 20px;
    right: 20px;
    border: 1.5px solid ${props=>props.theme.colors.red};
    outline: none;
    border-radius: 29px;
    cursor: pointer;
    -webkit-animation: ${bounceAnimation} 0.3s infinite alternate ease-in-out;
    -moz-animation: ${bounceAnimation}  0.3s infinite alternate ease-in-out;
    animation: ${bounceAnimation}  0.3s infinite alternate ease-in-out;
    animation-iteration-count: 6;
    animation-iteration-count: ${ (props) => (props.clicked) ? 0 : 'infinite'};
    &:hover {
        background: ${props=>props.theme.colors.darkRed};
        color: ${props => props.theme.colors.white};
        -webkit-animation: ${ (props) => (props.clicked) ? '': css`${bounceAnimation}  0.3s infinite alternate ease-in-out`};
        -moz-animation: ${ (props) => (props.clicked) ? '': css`${bounceAnimation}  0.3s infinite alternate ease-in-out`};
        animation: ${ (props) => (props.clicked) ? '': css`${bounceAnimation}  0.3s infinite alternate ease-in-out`};
    }
    :before {
        content: ${ (props) => (props.clicked) ? '\'\\d7\'' : '\'Q\''};
    }
`;

const ChatbotContainer = ({display, started, triggerStartChatbot, triggerToggleChatbot}) => {

    const handleToggleChatbot = () => {
        triggerStartChatbot();
        triggerToggleChatbot();
    }

    return (
        <>  
            <ChatModal />
            <ChatBtn onClick = {handleToggleChatbot} clicked = {display} /> 
        </>
    )
}

const mapStateToProps = ({chatbot}) => ({
    display: chatbot.display,
    started: chatbot.started,
})

const mapDispatchToProps = (dispatch) => ({
    triggerStartChatbot: () => { dispatch(startChatbot()) },
    triggerToggleChatbot: () => { dispatch(toggleChatbot()) }
})
 
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ChatbotContainer);