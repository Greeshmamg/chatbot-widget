import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import MessageList from '../MessageList';
import PromotionBanner from '../PromotionBanner';
import { toggleChatbot } from '../../store/actions/chatbotActions';
import ChatInput from '../ChatInput';

const ModalContainer = styled.div`
    position: fixed;
    right: 30px;
    bottom: 30px;
    height: 95%;
    height: ${ (props) => (props.active) ? '95%' : '10%'};
    width: ${ (props) => (props.active) ? props.theme.widths.chatbotPanel : 0};
    opacity: ${ (props) => (props.active) ? 1 : 0};
    max-width: 80%;
    background: ${props=>props.theme.colors.lightGrey} !important;
    box-sizing: border-box;
    border: 2px solid ${props=>props.theme.colors.midGrey};
    border-radius: 4px;
    box-shadow: 3px 6px 12px 0.5px ${props=>props.theme.colors.darkGrey};
    transition: all .4s ease-in-out;
    display: flex;
    flex-direction: column;
`;

const TitleBarWrapper = styled.div`
    background: ${props=>props.theme.colors.darkRed};
    border: 2px solid ${props=>props.theme.colors.darkRed};
    border-radius: 4px 4px 0 0;
    box-sizing: border-box;
    align-items: center;
    justify-content: space-between;
    height: 70px;
    padding-left: 15px;
    margin-top: 1px;
    color: ${props=>props.theme.colors.lightGrey};
    display: flex;
`;

const Title = styled.div`
    font-weight: normal;
    font-size: 27px;
    position: relative;
`;

const CloseBtn = styled.button`
    background: transparent;
    color: ${ (props) => props.theme.colors.midGrey};
    font-size: 32px;
    height: 32px;
    width: 32px;
    position: absolute;
    top: 10px;
    right: 10px;
    border: none;
    outline: none;
    cursor: pointer;
    &:hover {
        color: ${props => props.theme.colors.white};
        text-shadow: 1px 1px 8px ${props => props.theme.colors.lightGrey};
    }
    :before {
        content: '\\d7';
    }
`;

const ChatModal = ({display, sendUserQuery, triggerToggleChatbot}) => {

    return (
        <>
            <ModalContainer active={display}>
                <TitleBarWrapper>
                    <Title>QBR</Title>
                    <CloseBtn onClick={triggerToggleChatbot} />
                </TitleBarWrapper>
                <MessageList />
                    <ChatInput />
                <PromotionBanner />
            </ModalContainer>
        </>
    )
}

const mapStateToProps = ({chatbot}) => ({
    display: chatbot.display,
})

const mapDispatchToProps = (dispatch) => ({
    triggerToggleChatbot: () => { dispatch(toggleChatbot()) }
})

export default connect(mapStateToProps, mapDispatchToProps)(ChatModal);