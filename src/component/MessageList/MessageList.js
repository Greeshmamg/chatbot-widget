import React, {useEffect, useRef} from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import ResponseCard from '../ResponseCard';
import LoadingCard from '../ResponseCard/LoadingCard';
import Session from '../../utils/Session';

const MessageListContainer =  styled.div`
    display: flex;
    flex-direction: column-reverse;
    flex: 1 1;
    height: 85%;
    overflow-x: hidden;
    overflow-y: auto;
    ::-webkit-scrollbar {
        width: 8px;   
    }
    /* Track */
    ::-webkit-scrollbar-track {
        box-shadow: inset 0 0 5px grey; 
        border-radius: 10px;
        margin: 2px 0;
    }
    /* Handle */
    ::-webkit-scrollbar-thumb {
        background: ${props=>props.theme.colors.red};
        border-radius: 4px;
        &:hover {
            background: ${props=>props.theme.colors.darkRed};
        }
    }
`;

 const MessageListWrapper =  styled.div`
    padding: 10px;
    flex: 1;
    margin-bottom: 15px;
 `;

const MessageList = ({messageList, loading}) => {
    
    const messagesEndRef = useRef();

    const scrollToBottom = () => {
        const {current} = messagesEndRef;
        current.scrollTo(0,current.scrollHeight);
    }
  
    useEffect(() => {
        scrollToBottom();
        if (messageList && messageList.length) {
            //Session.setChatHistory(messageList);
        }
    }, [messageList]);

    return (
        <>
            <MessageListContainer ref={messagesEndRef} >
                <MessageListWrapper>
                    {
                        (messageList && messageList.length) ? (messageList.map(({type, messageObj}, index)=> {
                            return <ResponseCard key = {index} type={type} messageObj={messageObj} />;
                    })) :
                        <></>
                    }
                    {
                        loading ? <LoadingCard /> : <></>
                    }
                </MessageListWrapper>
            </MessageListContainer>
        </>
    )
};

const mapStateToProps = ({chatbot}) => ({
    messageList: chatbot.messageList,
    loading: chatbot.loading,
})

export default connect(mapStateToProps)(MessageList);