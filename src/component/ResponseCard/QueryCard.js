import React from 'react';
import styled from 'styled-components';

const DialogBaloon =  styled.div`
    align-self: flex-end;
    background-color: ${props=>props.theme.colors.red};
    color: ${props=>props.theme.colors.white};
    margin: 0;
    margin-left: 85px;
    padding: 10px 22px;
    border-radius: 12px 0px 12px 12px;
    max-width: calc(100% - 28px);
    word-break: break-word;
    word-wrap: break-word;
`;

const QueryCard = ({query}) => {
    return <DialogBaloon>{query}</DialogBaloon>;
};

export default QueryCard;