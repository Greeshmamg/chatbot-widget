import React from 'react';
import styled from 'styled-components';

const DialogBaloon =  styled.div`
    align-self: flex-start;
    background-color: ${props => props.theme.colors.white};
    margin: 0;
    margin-right: 85px;
    padding: 8px 22px;
    border-radius: 0px 12px 12px 12px;
    max-width: calc(100% - 28px);
    word-break: break-word;
    word-wrap: break-word;
`;

const LineWrapper = styled.div`
    padding: 4px 0;
    line-height: 1.2em;
`;

const TextResponseCard = ({textMessages}) => {
    return (
        <DialogBaloon>
            {
                textMessages.map((textMessage, index) => {
                    return <LineWrapper key={index}>{textMessage}</LineWrapper>
                })
            }
        </DialogBaloon>
    )
};

export default TextResponseCard;