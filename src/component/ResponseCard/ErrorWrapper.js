import React, { Component } from 'react'
import ErrorCard from './ErrorCard';

export default class ErrorWrapper extends Component {

    state = { hasError: false };
      
    static getDerivedStateFromError(error) {
        console.log(error);
        return { hasError: true } ;
    }

    render() {
        //console.log(this.state.hasError);
        if (this.state.hasError) {
            return <ErrorCard />
        };
        return this.props.children;
    }
};