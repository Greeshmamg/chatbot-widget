import React from 'react';
import styled from 'styled-components';

const DialogBaloon =  styled.div`
    align-self: flex-start;
    background-color: #fff;
    display: flex;
    flex-direction: column;
    margin: 0;
    margin-right: 85px;
    padding: 0;
    padding-top: 8px;
    border-radius: 0px 12px 12px 12px;
    max-width: calc(100% - 28px);
    word-break: break-word;
    word-wrap: break-word;
`;

const SectionWrapper = styled.div`
    padding: 2px 22px;
`;

const RedBtn = styled.a`
    display: block;
    margin: 10px auto;
    font-size: 18px;
    text-align: center;
    color: ${props => props.theme.colors.white};
    border: 1px solid ${props=>props.theme.colors.red} !important;
    border-radius: 4px;
    outline: none;
    cursor: pointer;
    background: ${props=>props.theme.colors.red};
    padding: 10px 10px;
    text-decoration: none;
    text-transform: uppercase;
    transition: background 0.1s ease,color 0.1s ease;
    &:hover {
        background-color: ${props=>props.theme.colors.darkRed};
  }
`;

const LineWrapper = styled.div`
    padding: 4px 0;
    line-height: 1.2em;
`;

const LinksResponseCard = ({payload}) => {
    const {buttons, messages} = payload.fields;
    return (
        <DialogBaloon>
            <SectionWrapper>
                {
                    messages.listValue.values ? messages.listValue.values.map((message, index) => {
                        return <LineWrapper key={index}>{message.stringValue}</LineWrapper>
                    }) :
                    <></>
                }
            </SectionWrapper>
            <SectionWrapper>
                {
                    buttons.listValue.values ? buttons.listValue.values.map((button, index) => {
                        const label = button.structValue.fields.buttonLabel.stringValue;
                        const link = button.structValue.fields.buttonLink.stringValue;
                        return <RedBtn key={index} target={(link.endsWith('/login')) ? '' : '_blank'} href={link} >{label}</RedBtn>
                    }) :
                    <></>
                }
            </SectionWrapper>
        </DialogBaloon>
    )
};

export default LinksResponseCard;