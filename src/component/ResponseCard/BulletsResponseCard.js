import React from 'react';
import styled from 'styled-components';

const DialogBaloon =  styled.div`
    align-self: flex-start;
    background-color: #fff;
    display: flex;
    flex-direction: column;
    margin: 0;
    margin-right: 85px;
    padding: 0;
    padding-top: 8px;
    border-radius: 0px 12px 12px 12px;
    max-width: calc(100% - 28px);
    word-break: break-word;
    word-wrap: break-word;
`;

const SectionWrapper = styled.div`
    padding: 2px 22px;
`;

const BulletPointsWrapper = styled.div`
    margin: 5px;
    padding: 2px 22px;
`;

const LineWrapper = styled.li`
    padding: 4px 0;
    line-height: 1.2em;
`;

const BulletsResponseCard = ({payload}) => {
    const {title, listItems} = payload.fields;
    return (
        <DialogBaloon>
            <SectionWrapper>
                {title.stringValue}
            </SectionWrapper>
            <BulletPointsWrapper>
                {
                    listItems.listValue.values ? listItems.listValue.values.map((listItem, index) => {
                        return <LineWrapper key={index} >{listItem.stringValue}</LineWrapper>
                    }) :
                    <></>
                }
            </BulletPointsWrapper>
        </DialogBaloon>
    )
};

export default BulletsResponseCard;