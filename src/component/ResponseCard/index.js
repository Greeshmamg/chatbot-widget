import React, {Component} from 'react'
import styled from 'styled-components';
import QueryCard from './QueryCard';
import TextResponseCard from './TextResponseCard';
import QuickRepliesResponseCard from './QuickRepliesResponseCard';
import LinksResponseCard from './LinksResponseCard';
import ErrorCard from './ErrorCard';
import { MESSAGE_TYPE, RESPONSE_TYPE } from '../../utils/Constants';
import ErrorWrapper from './ErrorWrapper';
import BulletsResponseCard from './BulletsResponseCard';

const CardWrapper =  styled.div`
    display: flex;
    flex-direction: column;
    font-size:15px;
    margin: 10px 0;
`;

export default class ResponseCard extends Component {

    render() {
        const {type, messageObj} = this.props;

        const getResponseCard = (type, messageObj) => {
            if (type===MESSAGE_TYPE.QUERY) { return <QueryCard query={messageObj}/> };
            if (type===MESSAGE_TYPE.RESPONSE && messageObj.message===RESPONSE_TYPE.TEXT) { return <TextResponseCard textMessages={messageObj.text.text}/> };
            if (type===MESSAGE_TYPE.RESPONSE && messageObj.message===RESPONSE_TYPE.QUICK_REPLIES) { return <QuickRepliesResponseCard quickReplies={messageObj.quickReplies}/> };
            if (type===MESSAGE_TYPE.RESPONSE && messageObj.message===RESPONSE_TYPE.PAYLOAD && messageObj.payload.fields.buttons) { return <LinksResponseCard payload={messageObj.payload}/> };
            if (type===MESSAGE_TYPE.RESPONSE && messageObj.message===RESPONSE_TYPE.PAYLOAD && messageObj.payload.fields.listItems) { return <BulletsResponseCard payload={messageObj.payload}/> };
            //if (type===MESSAGE_TYPE.ERROR) { return <ErrorCard error={ERROR_TYPE.SERVER_ERROR_OCCURED} />};
            return <ErrorCard />; 
        }
        
        return (
            <CardWrapper>
                <ErrorWrapper>
                    { getResponseCard(type, messageObj) }
                </ErrorWrapper>
            </CardWrapper>
        )
    }
}
