import React from 'react';
import styled from 'styled-components';
import loading_icon from '../../img/loading_icon.gif';

const DialogBaloon =  styled.div`
    align-self: flex-start;
    background-color: ${props => props.theme.colors.white};
    height: 22px;
    width: 38px;
    overflow: hidden;
    margin: 0;
    margin-right: 85px;
    padding: 8px 22px;
    border-radius: 0px 12px 12px 12px;
    max-width: calc(100% - 28px);
    position: relative;
`;

const LoadingIcon = styled.img.attrs({
    src: loading_icon,
    alt: 'loading'
})`
    position: absolute;
    height: 52px;
    width: auto;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
`;

const LoadingCard = () => {
    return (
        <DialogBaloon>
            <LoadingIcon />
        </DialogBaloon>
    )
};

export default LoadingCard;