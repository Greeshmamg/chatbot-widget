import React from 'react';
import styled from 'styled-components';
import {connect} from 'react-redux';
import { addQueryToConversation } from '../../store/actions/conversationActions';

const DialogBaloon =  styled.div`
    background-color: ${props => props.theme.colors.white};
    display: flex;
    flex-direction: column;
    margin: 0;
    margin-right: 85px;
    padding: 0;
    padding-top: 8px;
    border-radius: 0px 12px 12px 12px;
    max-width: calc(100% - 28px);
    word-break: break-word;
    word-wrap: break-word;
`;

const QuickReplyTitle = styled.div`
    padding: 2px 22px;
`;

const QuickReplyOptionsWrapper = styled.div`
    padding: 0;
    margin-top: 10px;
    border-radius: 0px 0px 12px 12px;
`;

const QuickReplyOption = styled.div`
    padding: 8px 22px;
    border-top: 1px solid ${props => props.theme.colors.white};
    cursor: pointer;
    background-color: ${props=>props.theme.colors.midGrey};
    text-align: center;
    :hover {
        /* border: 1px solid ${props => props.theme.colors.red}; */
        font-weight: 700;
    }
    :last-child {
        border-radius: 0px 0px 12px 12px;
    }
`;

const QuickRepliesResponseCard = ({quickReplies, sendUserQuery}) => {

    return (
        <DialogBaloon>
            <QuickReplyTitle>{quickReplies.title}</QuickReplyTitle>
            <QuickReplyOptionsWrapper>
            {
                quickReplies.quickReplies ? quickReplies.quickReplies.map((quickReply, index) => (
                    <QuickReplyOption key={index} onClick={ (e) => { sendUserQuery(e.target.innerText) } }>{quickReply}</QuickReplyOption>
                ))
                :
                <></>
            }
            </QuickReplyOptionsWrapper>
        </DialogBaloon>
    )
};

const mapDispatchToProps = (dispatch) => ({
    sendUserQuery: (userQuery) => { dispatch(addQueryToConversation(userQuery)) }
})

export default connect(null, mapDispatchToProps)(QuickRepliesResponseCard);