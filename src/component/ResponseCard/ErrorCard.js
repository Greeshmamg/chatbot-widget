import React from 'react';
import styled from 'styled-components';

const DialogBaloon =  styled.div`
    align-self: flex-start;
    background-color: #fff;
    display: flex;
    flex-direction: column;
    margin: 0;
    margin-right: 85px;
    padding: 0;
    padding-top: 8px;
    border-radius: 0px 12px 12px 12px;
    max-width: calc(100% - 28px);
    word-break: break-word;
    word-wrap: break-word;
`;

const SectionWrapper = styled.div`
    padding: 2px 22px;
`;

const LineWrapper = styled.div`
    padding: 4px 0;
    line-height: 1.2em;
`;

const RedBtn = styled.a`
    display: block;
    margin: 10px auto;
    font-size: 18px;
    text-align: center;
    color: ${props => props.theme.colors.white};
    border: 1px solid ${props=>props.theme.colors.red} !important;
    border-radius: 4px;
    outline: none;
    cursor: pointer;
    background: ${props=>props.theme.colors.red};
    padding: 10px 10px;
    text-decoration: none;
    text-transform: uppercase;
    transition: background 0.1s ease,color 0.1s ease;
    &:hover {
        background-color: ${props=>props.theme.colors.darkRed};
  }
`;


const ErrorCard = () => {
    const buttons = [
        {
          label: 'LIVE PERSON CHAT',
          link: 'https://www.qantas.com/au/en/business-rewards/qbr/live-person-chatting',
        },
        {
          label: 'CONTACT US',
          link: 'https://www.qantas.com/au/en/business-rewards/qbr/contactus',
        },
      ];
    const messages = [
        'Opps! Something went wrong.',
        'You may click on the below \'LIVE PERSON CHAT\' button for our dedicated customer care service team.',
        'Alternatively you can also submit your query by using our contact us form by clicking on the \'CONTACT US\' button below',
      ];
    return (
        <DialogBaloon>
            <SectionWrapper>
                {
                    messages ? messages.map((message, index) => {
                        return <LineWrapper key={index}>{message}</LineWrapper>
                    }) :
                    <></>
                }
            </SectionWrapper>
            <SectionWrapper>
                {
                    buttons ? buttons.map((button, index) => {
                        return <RedBtn key={index} target='_blank' href={button.link} >{button.label}</RedBtn>
                    }) :
                    <></>
                }
            </SectionWrapper>
        </DialogBaloon>
    )
};

export default ErrorCard;