import {take, call, takeLatest} from 'redux-saga/effects';
import {CHATBOT, MESSAGE_LIST} from '../constants';
import {handleChatbotLoad, handleUserInput} from './conversationSaga';

function* chatbotLoadWatcher() {
    yield take(CHATBOT.START);
    yield call(handleChatbotLoad);
}

function* rootSaga() {
    yield call(chatbotLoadWatcher);
    yield takeLatest(MESSAGE_LIST.ADD_QUERY, handleUserInput);
}

export default rootSaga;