import {put, call } from 'redux-saga/effects';
import {addResponseToConversation, addErrorToConversation, setMessageList} from '../actions/conversationActions';
import {getDialogFlowResponse, getDialogFlowWelcomeResponse} from '../../services';
import Session from '../../utils/Session';
import { startChatbotLoading } from '../actions/chatbotActions';

//export const getIsChatbotStarted = (state) => state.chatbot.started;

function sleep(milliSeconds) {
    return new Promise((resolve, reject) => {
        setTimeout(resolve, milliSeconds);
    });
};

export function* handleChatbotLoad() {
    const chatHistory = Session.getChatHistory();
    if (chatHistory && chatHistory.length) {
        yield put(setMessageList(chatHistory));
        return;
    }
    try {
        const responses = yield call(getDialogFlowWelcomeResponse);
        for (let response of responses) {
            yield put(startChatbotLoading());
            yield call(sleep, 2000);
            yield put(addResponseToConversation(response));
        };
    } catch (error) {
        yield put(addErrorToConversation(error));
    }
}

export function* handleUserInput({query}) {
    try {
        const responses = yield call(getDialogFlowResponse, query);
        for (let response of responses) {
            yield put(startChatbotLoading());
            yield call(sleep, 800);
            yield put(addResponseToConversation(response));
        };
    } catch (error) {
        yield put(addErrorToConversation(error));
    }
};