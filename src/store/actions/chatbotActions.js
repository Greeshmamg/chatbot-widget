import {CHATBOT} from '../constants'

export const startChatbot = () => ({
    type: CHATBOT.START,
})

export const toggleChatbot = () => ({
    type: CHATBOT.TOGGLE,
})

export const startChatbotLoading = () => ({
    type: CHATBOT.START_LOADING,
})