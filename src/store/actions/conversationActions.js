import {MESSAGE_LIST} from '../constants'

export const setMessageList = messageList => ({
    type: MESSAGE_LIST.SET_LIST,
    messageList
})

export const addQueryToConversation = query => ({
    type: MESSAGE_LIST.ADD_QUERY,
    query
})

export const addResponseToConversation = response => ({
    type: MESSAGE_LIST.ADD_RESPONSE,
    response
})

export const addErrorToConversation = error => ({
    type: MESSAGE_LIST.ADD_ERROR,
    error
});