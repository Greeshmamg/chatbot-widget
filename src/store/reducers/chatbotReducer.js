import {MESSAGE_LIST, CHATBOT} from '../constants';
import {MESSAGE_TYPE} from '../../utils/Constants'

const initState = {
    messageList: [],
    started: false,
    display: false,
    loading: false
}

const conversationReducer = (state = initState, action) => {
    let message;
    switch(action.type) {
        case (CHATBOT.START):
            return {
                ...state,
                started: true,
                loading: true,
            }

        case (CHATBOT.TOGGLE):
            return {
                ...state,
                display: !state.display
            }
            
        case (CHATBOT.START_LOADING):
            return {
                ...state,
                loading: true
            }

        case (MESSAGE_LIST.SET_LIST):
            const {messageList} = action;
            return {
                ...state,
                messageList,
                loading: false,
            }

        case (MESSAGE_LIST.ADD_QUERY):
            message = { 
                type: MESSAGE_TYPE.QUERY,
                messageObj: action.query
            }
            return {
                ...state,
                messageList: [...state.messageList, message],
                loading: true,
            }

        case (MESSAGE_LIST.ADD_RESPONSE):
            message = {
                type: MESSAGE_TYPE.RESPONSE,
                messageObj: action.response
            };
            return {
                ...state,
                messageList: [...state.messageList, message],
                loading: false,
            }

        case (MESSAGE_LIST.ADD_ERROR):
            message = {
                type: MESSAGE_TYPE.ERROR,
                messageObj: action.error
            }
            return {
                ...state,
                messageList: [...state.messageList, message],
                loading: false,
            }

        default:
            return state
    }
} 

export default conversationReducer;