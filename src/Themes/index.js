export default {
    widths: {
        chatbotPanel: "510px"
    },
    fonts: {
        family:"sans-serif"
    },
    colors: {
        lightGrey: "#f4f5f6",
        midGrey: "#ccc",
        darkGrey: "#323232",
        white: "#fff",
        mainFont: "#323232",
        red: "rgb(228, 0, 0)",
        darkRed: "rgb(174, 0, 0)",
    },
    fontSizes: {
        button: "10px",
    },    
}