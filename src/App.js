import React from 'react';
import configureStore from './store';
import { createGlobalStyle, ThemeProvider} from 'styled-components';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Theme from "./Themes";
import {Provider} from 'react-redux';
import Chatbot from './component/Chatbot';

const GlobalStyles = createGlobalStyle`
  *{
  box-sizing: border;
  margin: 0;
  padding: 0;
  };
  body, html {
  font-family: ${props => props.theme.fonts.family};
  color: ${props => props.theme.colors.mainFont};
  };
`;

const store = configureStore();

function App() {
  return (
  <Provider store={store}>
    <ThemeProvider theme={Theme}>
      <GlobalStyles />
      <BrowserRouter>
          <Switch>
            <Route exact path='/' component={ Chatbot } />
            <Route exact path='/dashboard' component={ Chatbot } />
          </Switch>
        </BrowserRouter>
    </ThemeProvider>
  </Provider>
  );
}

export default App;
