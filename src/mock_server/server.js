const express = require('express');
const cors = require('cors');
const mockEventResponse = require('./responses/mockEventResponse.json');
const mockTextResponse = require('./responses/mockTextResponse.json');

const server = express();
const PORT = 3001;

server.use(cors());

server.post('/getResponse/event', (_req, res) => {
  setTimeout(() => res.status(200).json(mockEventResponse), 1000)
});

server.post('/getResponse/text', (_req, res) => {
  setTimeout(() => res.status(200).json(mockTextResponse), 1000)
});

// eslint-disable-next-line no-console
server.listen(PORT, () => console.log(`Mock server is running on port ${PORT}!`));
