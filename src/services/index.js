import request from 'superagent';
import { SERVER_BASE_URL } from '../utils/Configs';
import Session from '../utils/Session';

const getAuthHeader = () => Session.getToken() ? {'Authorization' : ("Bearer " + Session.getToken())} : {};

export const getDialogFlowResponse = async (query) => {
  const req = {
    text: query,
    sessionId: Session.getSessionId()
  };
  //beforeSend(request);
  const response = await request.post(SERVER_BASE_URL+'/getResponse/text', req).set('Authorization', "Bearer " + Session.getToken());
  Session.setSessionId(response.body.sessionId);
  return response.body.fulfillmentMessages;
}

export const getDialogFlowWelcomeResponse = async () => {
  const req = {
    event: "WelcomeToQBR",
    sessionId: Session.getSessionId()
  };
  //beforeSend(request);
  const response = await request.post(SERVER_BASE_URL+'/getResponse/event', req).set('Authorization', "Bearer " + Session.getToken());

  if(Session.getToken()){
    return response.body.fulfillmentMessages.filter((v, i) => i !== 1);
  }

  Session.setSessionId(response.body.sessionId);
  return response.body.fulfillmentMessages;
}