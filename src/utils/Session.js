class Session {

    setSessionId = (sessionId) => sessionStorage.setItem('SESSION_ID', sessionId);

    getSessionId = () => sessionStorage.getItem('SESSION_ID');

    //getToken = () => 'SAMPLE-TOKEN';
    getToken = () => sessionStorage.getItem('AUTH_TOKEN');
    
    setChatHistory = (messageList) => sessionStorage.setItem('CHAT_HISTORY', JSON.stringify(messageList));

    getChatHistory = () => JSON.parse(sessionStorage.getItem('CHAT_HISTORY'));

    clearSession = () => sessionStorage.clear();
}

export default new Session();