export const MESSAGE_TYPE = {
    QUERY: 'QUERY',
    RESPONSE: 'RESPONSE',
    ERROR: 'ERROR',
}

export const RESPONSE_TYPE = {
    TEXT: 'text',
    PAYLOAD: 'payload',
    QUICK_REPLIES: 'quickReplies',
}

export const ERROR_TYPE = {
    SERVER_ERROR_OCCURED: 'SERVER ERROR OCCURED',
    UPSUPPORTED_RESPONSE_RECEIVED: 'UPSUPPORTED RESPONSE RECEIVED',
}

export const PROMOTION_BANNER_TEXT = 'This is a place holder for details about latest promotions and active campaign details';

export const AUTO_SUGGEST_LIST=[ 'How can I create a new QBR account?',
    'How can I join in Qantas Business rewards program?',
    'How can I get access to an existing QBR account?',
    'How many points did I earn',
    'I am unable to see my transactions.',
    'I would like to update my account details.',
    'I would like to see my transactions.',
    'I would like to add a new user to my account.',
    'I would like to invite a new user to my account.',
    'I would like to create a new user in my account.',
    'I would like to create a new nominee in my account.',
    'I would like to add a new traveller to my account.',
    'I would like to update traveller details in my account.',
    'I would like to delete travellers in my account.',
    'I would like to see all travellers in my account.',
    'I would like to see all the active campaigns',
    'I would like to see all the registered campaigns',
   ' I would like to see all the partner campaigns',
    'I would like to change the account holder for my account.',
    'I would like to change the account manager for my account.',
    'I would like to know how long does it take to credit points for my last transaction?',  
    'I would like to know how to transfer points from my account',
    'I am unable to activate my account.',
    'I am unable to log into my account.',
    'I would like to see all permissions of a user',
    'I would like to add a permission to a user',
    'I would like to remove a permission of a user.',
    'I would like to update permissions of a user.',
    'I would like to see all users',
    'I would like to activate a user',
   'I would like to deactivate a user',
    'I would like to reactivate a user',
    'I would like to approve a pending request'];